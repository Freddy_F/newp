'use strict';

/**
 * @ngdoc overview
 * @name newpApp
 * @description
 * # newpApp
 *
 * Main module of the application.
 */
angular
  .module('newpApp', [
    'ngAnimate',
    'ngCookies',
    'ngRoute',
    'ngTouch',
    'ui.bootstrap',
    'ui.slider',
    'angularMoment',
    'toastr'
  ]).config(function($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      }).when('/signin', {
        templateUrl: 'views/signin.html',
        controller: 'SigninCtrl'
      }).when('/signup', {
        templateUrl: 'views/signup.html',
        controller: 'SigninCtrl'
      }).when('/forgot', {
        templateUrl: 'views/forgot.html',
        controller: 'AboutCtrl'
      }).when('/lock', {
        templateUrl: 'views/lock.html',
        controller: 'AboutCtrl'
      }).when('/game/:gameid', {
        templateUrl: 'views/game.html',
        controller: 'GameCtrl'
      }).when('/team', {
        templateUrl: 'views/team.html',
        controller: 'TeamCtrl'
      }).when('/data', {
        templateUrl: 'views/data.html',
        controller: 'DataCtrl'
      }).when('/record', {
        templateUrl: 'views/record.html'
      }).when('/prefer', {
        templateUrl: 'views/prefer.html',
        controller:'PreferCtrl'
      }).when('/deposit', {
        templateUrl: 'views/deposit.html',
        controller:'DepositCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  }).run(function(amMoment) {
        amMoment.changeLocale('zh-cn');
    }).directive('selectOnClick', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            element.on('click', function () {
                this.select();
            });
        }
    };
});
 