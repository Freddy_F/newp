'use strict';
angular.module('newpApp')
  .controller('GameCtrl', function($http, $scope, $rootScope, $routeParams, $log, $interval, $timeout, $filter, repeatGet, toastr) {
    $rootScope.cur.nav = 'game';
    $scope.Math = window.Math;

    $http.get('/lobby/app/kirinResource/globleResource/paihang').success(function(r) {
      var data = JSON.parse(r.data.content);
      $scope.awards = data.new;
      setTimeout(function() {
        $scope.$apply(function() {
          window.funnyNewsTicker = window.$('#news').funnyNewsTicker({});
          window.newsmoving = $scope.newsmoving = true;
        });
      }, 500);
    });

    $log.debug('routeParams', $routeParams);

    function init() {
      $http.get('/lotto/rest/period/' + $scope.lotto.label + '/previous').success(function(data) {
        $scope.previous = data;
        if (!$scope.previous.numbers) {
          lottoing();
        }
      });
      $http.get('/lotto/rest/period/' + $scope.lotto.label).success(function(data) {
        $scope.curPeriod = data;
      });
      $http.get('/lotto/rest/period/' + $scope.lotto.label + '/next').success(function(data) {
        $log.log('下一期', data, status);
        $scope.next = data;
      });
      $http.get('/lotto/rest/period/' + $scope.lotto.label + '/recent/8')
        .success(function(data) {
          $scope.recent = data;
        });
        // $('.ui-slider-handle').draggable();
      $http.get('/lotto/rest/brokerage/?type=' + $scope.lotto.label).success(function(brokerage) {
        $scope.selBrokerage = {
          val: brokerage.upper * 2,
          min: 1700,
          max: brokerage.upper * 2
        };
      });
    }

    function change() {
      $scope.previous = $scope.curPeriod;
      $scope.curPeriod = $scope.next;
      lottoing(true);
    }

    $scope.$on('$destroy', function() {
      // Make sure that the interval is destroyed too
      $interval.cancel($scope.timer);
      repeatGet.clear();
    });
    $scope.timer = $interval(function() {
      if ($scope.lotto.label === 'MMC') {
        return false;
      }
      var now = window.moment().toDate().getTime();
      var open = window.moment($scope.curPeriod.openTime).toDate().getTime();
      // $log.debug((now.getTime() - open.getTime()) / 1000);
      if ((open - now) / 1000 < $scope.curPeriod.game.offSaleAdvance) {
        change();
        var audio = document.getElementById('kaijiang_audio');
        audio.currentTime = 0;
        audio.play();
        // Sometime Later 兼容 mobile
        audio.src = 'kaijiang.mp3';
        audio.play();
      }
      $scope.countDown = Number((open - now) / 1000 - $scope.curPeriod.game.offSaleAdvance).formatTime();
    }, 1000);

    function lottoing(delay) {
      repeatGet.getNumbers('/lotto/rest/period/' + $scope.lotto.label + '/single/' + $scope.previous.signature, 5000, 60, delay ? $scope.previous.game.offSaleAdvance * 1000 : 0).then(function(data, status) {
          $log.log('上一期', data, status);
          $scope.previous = data;
          $http.get('/lotto/rest/period/' + $scope.lotto.label + '/next').success(function(data, status) {
            $log.log('下一期', data, status);
            $scope.next = data;
          });
          $http.get('/lotto/rest/period/' + $scope.lotto.label + '/recent/8')
            .success(function(data) {
              $scope.recent = data;
            });
        },
        function(data, status) {
          $log.log(data, status);
        });
    }
    $scope.selectLotto = function(lotto) {
      $scope.lotto = window.lottos[lotto];
      $rootScope.cur.lotto = lotto;
      $scope.currentTab = $scope.lotto.tabs[0];
      $scope.currGroup = $scope.currentTab.methodgroups[0];
      $scope.currentMethod = $scope.currentTab.methodgroups[0].methods[0];
      init();
    };
    $scope.selectLotto($routeParams.gameid);
    $scope.selTab = function(tab) {
      $scope.currentTab = tab;
      $scope.currGroup = $scope.currentTab.methodgroups[0];
      $scope.currentMethod = $scope.currGroup.methods[0];
    };
    $scope.selMethod = function(group, method) {
      $scope.currGroup = group;
      $scope.currentMethod = method;
    };
    $scope.curTicket = {}; //init
    $scope.selNum = function(row, n) {
      // $log.debug(row, n);
      if (!$scope.curTicket[row.name]) {
        $scope.curTicket[row.name] = {};
      }
      if ($scope.curTicket[row.name][n.value] === true) {
        $scope.curTicket[row.name][n.value] = false;
      } else {
        $scope.curTicket[row.name][n.value] = true;
      }
      // $log.debug($scope.curTicket);
      $scope.curLetter();
    };
    $scope.selAll = function(row) {
      if (!$scope.curTicket[row.name]) {
        $scope.curTicket[row.name] = {};
      }
      angular.forEach(row.nums, function(n) {
        $scope.curTicket[row.name][n.value] = true;
      });
      $scope.curLetter();
    };
    $scope.selBig = function(row) {
      $scope.curTicket[row.name] = {};
      angular.forEach(row.nums, function(n) {
        $scope.curTicket[row.name][n.value] = n.value > $scope.lotto.midNum;
      });
    };
    $scope.selSmall = function(row) {
      $scope.curTicket[row.name] = {};
      angular.forEach(row.nums, function(n) {
        $scope.curTicket[row.name][n.value] = n.value <= $scope.lotto.midNum;
      });
    };
    $scope.selOdd = function(row) {
      $scope.curTicket[row.name] = {};
      angular.forEach(row.nums, function(n) {
        $scope.curTicket[row.name][n.value] = n.value % 2 === 1;
      });
    };
    $scope.selEven = function(row) {
      $scope.curTicket[row.name] = {};
      angular.forEach(row.nums, function(n) {
        $scope.curTicket[row.name][n.value] = n.value % 2 === 0;
      });
    };
    $scope.selNone = function(row) {
      $scope.curTicket[row.name] = {};
      angular.forEach(row.nums, function(n) {
        $scope.curTicket[row.name][n.value] = false;
      });
    };
    $scope.$watch('curLetter()', function() {
      $scope.checkSelectingLegal();
    });
    $scope.checkSelectingLegal = function(imediate) {
      function check() {
        $log.log('checkSelectingLegal');
        var t = $scope.curLetter();
        var regx = $scope.currentMethod.regx;
        $log.log(regx, t);
        if (regx.test(t)) {
          $scope.selectingIsLegal = {};
          $http.post('/lotto/rest/ticket/quantity', {
              originalNumbers: t,
              game: $scope.lotto.label,
              method: $scope.currentMethod.code,
              multi: 1,
              mode: $scope.mode
            })
            .success(function(data) {
              $scope.selectingQty = data;
            }).error(function() {
              $scope.selectingQty = 0;
              $scope.selectingCanBuy = false;
            });
          $scope.selectingCanBuy = true;
          return true;
        } else {
          $scope.selectingQty = 0;
          $scope.selectingCanBuy = false;
          return false;
        }
      }
      if (imediate) {
        return check();
      }

      if ($scope.checkSelectingTimer) {
        $timeout.cancel($scope.checkSelectingTimer);
      }
      $scope.checkSelectingTimer = $timeout(function() {
        check();
      }, 600);
    };
    $scope.curLetter = function() {
      var letter = '';
      angular.forEach($scope.currentMethod.rows, function(row) {
        angular.forEach(row.nums, function(n) {
          if ($scope.curTicket[row.name] && $scope.curTicket[row.name][n.value] === true) {
            letter += n.value + ',';
          }
        });
        if (letter.endsWith(',')) {
          letter = letter.substring(0, letter.length - 1);
        }
        letter += ';';
      });
      letter = letter.substring(0, letter.length - 1);
      // $log.debug(letter);
      return letter;
    };
    $scope.checkCurrMultiple = function() {
      $scope.multi = $scope.multi.replace(/\D|^0/g, '');
      $scope.multi = $scope.multi > 65535 ? 65535 : $scope.multi;
      $scope.multi = $scope.multi < 1 ? 1 : $scope.multi;
    };
    $scope.selectingMny = function() {
      return $scope.selectingQty * 2 * Math.pow(0.1, $scope.mode) * $scope.multi;
    };
    $scope.tickets = [];
    $scope.add2basket = function() {
      var ticket = {
        title: $scope.currentTab.name + $scope.currGroup.name + '-' + $scope.currentMethod.name,
        method: $scope.currentMethod.code,
        num: $scope.curLetter(),
        multi: $scope.multi,
        mode: $scope.mode,
        qty: $scope.selectingQty,
        mny: $scope.selectingMny(),
        brokerage: ($scope.selBrokerage.max - $scope.selBrokerage.val) / 2
      };
      $scope.tickets.push(ticket);
    };
    $scope.removeTicket = function(index) {
      $scope.tickets.splice(index, 1);
    };
    $scope.$watchCollection('tickets', function(tickets) {
      if (tickets.length !== 1) {
        $scope.isChase = false;
      }
    });
    $scope.purchase = function() {
      $scope.trueTickets = [];
      var ticket = {
        originalNumbers: $scope.curLetter(),
        game: $scope.lotto.label,
        period: $scope.lotto.label !== 'MMC' ? $scope.curPeriod.signature : '',
        method: $scope.currentMethod.code,
        multi: $scope.multi,
        mode: $scope.mode,
        brokerage: ($scope.selBrokerage.max - $scope.selBrokerage.val) / 2
      };
      $scope.trueTickets.push(ticket);
      $http.post('/lotto/rest/ticket/batch', $scope.trueTickets).
      success(function(data) {
        if ($scope.lotto.label == 'MMC') {
          if (data[0].prize > 0) {
            toastr.success(data[0].prize+'元', '恭喜中奖', {
              closeButton: true,
              closeHtml: '<button></button>'
            });
          } else {
            toastr.info('您没中奖，请继续努力', '很遗憾', {
              closeButton: true,
              closeHtml: '<button></button>'
            });
          }
          init();
        }
      });
    };
    $scope.purchaseMulti = function() {
      $scope.trueTickets = [];
      if ($scope.isChase) {
        var t = $scope.tickets[0];
        var ticket = {
          originalNumbers: t.num,
          game: $scope.lotto.label,
          period: $scope.curPeriod.signature,
          method: t.method,
          multi: t.multi,
          mode: t.mode,
          brokerage: t.brokerage
        };
        $scope.trueTickets.push(ticket);
        var fps = $filter('limitTo')($scope.futurePeriods, $scope.chase.chaseCount);
        angular.forEach(fps, function(fp) {
          var ticket = {
            originalNumbers: t.num,
            game: $scope.lotto.label,
            period: fp.signature,
            method: t.method,
            multi: fp.chaseMulti,
            mode: t.mode,
            brokerage: t.brokerage
          };
          $scope.trueTickets.push(ticket);
        });
      } else {
        angular.forEach($scope.tickets, function(t) {
          var ticket = {
            originalNumbers: t.num,
            game: $scope.lotto.label,
            period: $scope.curPeriod.signature,
            method: t.method,
            multi: t.multi,
            mode: t.mode,
            brokerage: t.brokerage
          };
          $scope.trueTickets.push(ticket);
        });
      }


      var submitUrl = $scope.isChase ? '/lotto/rest/ticket/bundle' : '/lotto/rest/ticket/batch';
      var submitData = $scope.isChase ? {
        cancelOnWin: $scope.cancelOnWin,
        tickets: $scope.trueTickets
      } : $scope.trueTickets;
      $http.post(submitUrl, submitData).
      success(function(data) {

      });
    };
    $scope.getFuturePeriods = function() {
      $http.get('/lotto/rest/period/' + $scope.lotto.label + '/future/120')
        .success(function(data) {
          $scope.futurePeriods = data;
        });
    };
    $scope.$watch('curPeriod', function(currentPeriod) {
      if (currentPeriod) {
        $scope.getFuturePeriods();
      }
    });
    $scope.$watch('isChase', function(isChase) {
      if (isChase) {
        $scope.chase.chaseMulti = $scope.tickets[0].multi;
        $scope.cancelOnWin = false;
      }
    });

    function HaddleChase() {

      var fps = $filter('limitTo')($scope.futurePeriods, $scope.chase.chaseCount);
      $log.debug(fps);
      if ($scope.chase.chaseType === 'same') {
        angular.forEach(fps, function(fp) {
          fp.chaseMulti = $scope.chase.chaseMulti;
        });
      }
      if ($scope.chase.chaseType === 'times') {
        var times = $scope.chase.chaseTimes;
        angular.forEach(fps, function(fp, index) {
          fp.chaseMulti = times;
          if (index % $scope.chase.chaseGutter === 0) {
            times = times * $scope.chase.chaseTimes;
          }
        });
      }
    }
    $scope.$watch('chase', HaddleChase, true);
    $scope.$watchCollection('futurePeriods', HaddleChase);
  });