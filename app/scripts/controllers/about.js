'use strict';

/**
 * @ngdoc function
 * @name newpApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the newpApp
 */
angular.module('newpApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
