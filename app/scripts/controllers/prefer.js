'use strict';

/**
 * @ngdoc function
 * @name newpApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the newpApp
 */
angular.module('newpApp')
  .controller('PreferCtrl', function($scope, $rootScope, $http) {
    $rootScope.cur.nav='prefer';
    $http.get('/lobby/app/kirinResource/personal/personalInfo').success(function(r) {
      if (r.status === '1') {
        window.baseinfo = $rootScope.d.baseinfo = r.data;
      }
    });
    $http.get('/lobby/app/kirinResource/funds/getUserBindingBankCard').success(function(r) {
      $scope.cards = r.data || [];
    });
    $http.get('/lobby/app/kirinResource/funds/getSysBankInfo').success(function(r) {
      $scope.banks = r.data;
      $scope.selBank = $scope.banks[0];
    });
  });