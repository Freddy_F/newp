'use strict';
angular.module('newpApp').
controller('TeamCtrl', function($scope, $rootScope, $http, $log, $filter,toastr) {
  $rootScope.cur.nav = 'team';
  $scope.toggle = function(data) {
    data.show = !data.show;
  }
  $scope.delete = function(data) {
    data.nodes = [];
  };
  $scope.add = function(data) {
    if (!data.nodes) {
      data.nodes = [];
    }
    var post = (data.nodes || [1]).length + 1;
    var newName = data.userName + '-' + post;
    data.nodes.push({
      userName: newName,
      nodes: []
    });
  };
  $scope.hasNodes = function(data) {
    return (data.nodes.length > 0);
  };
  $scope.toggleTeamAdd = function() {
    $rootScope.v.teamadd = !$rootScope.v.teamadd;
  }
  $scope.selBrokerage = {
    val: 1956,
    max: 1956,
    min: 1700
  };
  // $('.ui-slider-handle').draggable();
  $scope.selBrokerage2 = {
    val: 1956,
    max: 1956,
    min: 1700
  };
  $scope.getTeam = function() {

    $http.get('/lobby/app/kirinResource/org/syncTeam').success(function(data) {
      $scope.tree = $scope.orgnizeData(data);
      $log.log($scope.tree);
    });
  };
  $scope.getTeam();
  $scope.do = function(form) {
    $log.log(form);
  };
  $scope.orgnizeData = function(thedata) {
    var data =angular.copy(thedata.data);
    var firsts = $filter('filter')(data,function(value,index){
      if(value.parentid===''){
        return true;
      }
    });
    iteNodes(firsts,data);
    return firsts;
  };

  function iteNodes(nodes,data) {
    angular.forEach(nodes, function(node) {
      node.nodes = $filter('filter')(data, {
        parentid: node.codeKey
      });
      iteNodes(node.nodes,data);
    });
  }
  $scope.addSubUser = function() {

    $scope.selQuota = {};
    $scope.selQuota.level = $scope.selBrokerage.val;
    $scope.selQuota.id = null;

    $http.post('/lobby/app/kirinResource/org/addSubUser', {
      userName: $scope.subUsername,
      newpass: $scope.subnewpass,
      role: 1,
      quota: $scope.selQuota
    }).success(function(r) {
      if (r.status === 'k') {
         toastr.success('添加用户成功', '温馨提示', {
          closeButton: true
        });
        $scope.getTeam();
        $rootScope.v.teamadd=false;
      } else {
       toastr.error(r.message, '温馨提示', {
          closeButton: true
        });
      }
    });
  };
});