'use strict';
angular.module('newpApp')
  .controller('DataCtrl', function($scope, $rootScope, $http,$log) {
    $rootScope.cur.nav = 'data';
    var moment = window.moment;
    $scope.dtfrom = moment().format('YYYY-MM-DD');
    $scope.dtto = moment().add(1, 'days').format('YYYY-MM-DD');

    $scope.open1 = function($event) {
      $event.preventDefault();
      $event.stopPropagation();

      $scope.fromopened = true;
    };
    $scope.open2 = function($event) {
      $event.preventDefault();
      $event.stopPropagation();

      $scope.toopened = true;
    };

    $scope.dateOptions = {
      formatYear: 'yy',
      startingDay: 1
    };

    $scope.formats = ['yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];
    var d = new Date();
    // d.setHours(12);
    // d.setMinutes( 0 );
    $scope.mytime =d;
    $scope.ismeridian = false;

    $scope.hstep = 1;
    $scope.mstep = 15;


    $scope.type = 'self';

    $scope.numPerPage = 10;
    $scope.numPerPageOpt = [5, 10, 20, 50, 100];
    $scope.boundaryLinks = true;
    $scope.directionLinks = true;
    $scope.filteredStores = {
      length: 100
    };
    $scope.noPrevious = function() {
      return $scope.page === 1;
    }
    $scope.noNext = function() {
      return $scope.page === $scope.pages.length;
    }
    $scope.pages = [{
      number: 1
    }, {
      number: 2
    }, {
      number: 3
    }, {
      number: 4
    }, {
      number: 5
    }, {
      number: 6
    }];
    $scope.selectPage = function(index) {
      if (index < 1 || index > $scope.pages.length) {
        return false;
      }
      $scope.page = index;
      angular.forEach($scope.pages, function(page) {
        page.active = false;
      });
      $scope.pages[index - 1].active = true;
      $log.log($scope.page);
    };
    $scope.selectPage(1);
    $scope.search=function(){
      $log.log($scope.dtfrom);
      $scope.dtfrom = window.moment($scope.dtfrom).format('YYYY-MM-DD');
      $log.log($scope.dtfrom);
      $log.log($scope.mytime);
      $log.log(window.prezero($scope.mytime.getHours()));
      $log.log(window.prezero($scope.mytime.getMinutes()));
    }
  });