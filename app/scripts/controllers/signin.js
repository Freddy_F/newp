'use strict';
angular.module('newpApp')
  .controller('SigninCtrl', function($scope, toastr,$log) {
    $scope.signin = function(form) {
      console.log(form);
      if ($scope.username === $scope.password) {
        toastr.success('非测试用户需要绑定您的银行卡和设置资金密码（初始：111111）!', '登录成功!', {
          closeButton: true
        });
        window.location.href = "/#/prefer";

      } else {
        toastr.error('用户名密码一致即可登录', '失败', {
          closeButton: true
        });

      }
    };
    $scope.signup = function(form) {
      console.log(form);
       toastr.success('请您登录', '注册成功', {
          closeButton: true
        });
        window.location.href = "/#/signin";
      return false;
    };
  });