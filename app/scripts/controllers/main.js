'use strict';

/**
 * @ngdoc function
 * @name newpApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the newpApp
 */
angular.module('newpApp')
  .controller('MainCtrl', function($rootScope, $scope, $http, $log, $location) {
    $rootScope.signin = function() {
      window.location.href = '/#/signin';
    }
    $rootScope.d = { //data

    };
    $rootScope.v = { //visible

    };
    $rootScope.cur = { //current

    };
    $http.get('/lobby/app/kirinResource/personal/personalInfo').success(function(r) {
      if (r.data !== null) {
        $log.log(r);
        $rootScope.d.user = r.data;
        $http.get('/lobby/app/kirinResource/globleResource/platform/carousel').success(function(r) {
          $rootScope.cnotices = r.data;
          $.each(r.data, function(i, n) {
            if (n.top === 1) {

              return false;
            }
          });
        });
      } else {
        if ($location.$$path === '/find') {
          return false;
        } else {

          setTimeout(function() {
            window.location.href = "/lobby/auth/a.html";
          }, 500);
        }
      }
    });
    window.GLO = function() {
      $.getJSON('/lobby/app/kirinResource/personal/logout', function(r) {
        alert('退出成功');
        if (r.status === '1') {
          setTimeout(function() {
            window.location = "/lotto/j_spring_cas_security_logout";
          }, 500);
        }
      });
    };
  });