window.fc3d={
	    	name:'福彩3D',
	    	label:'FC3D',
	    	cate:'3D',
	    	category:'game3d',
	    	number:3,
	    	maxNumber:9,
	    	totalPeriods:'每天一期',
	    	zoushi:'http://www.d5xq.cn/AloneModel/five.php?lotteryid=3&limit=30',
	    	tabs:[
		    	{
		    		name:'三星',
		    		label:'sanxing',
			    	methodgroups:[
			    		{
			    			name:'直选',
			    			methods:[
			    				{
			    					name:'直选复式',code:'3DC',tip:'从百位、十位、个位中选择一个3位数号码组成一注。',
			    					spec:'number',
			    					widget:true,
			    					regx:/^\d(,\d)*(;\d(,\d)*){2}$/,base:2000,
			    					rows:[
			    						{name:'百位',nums:[{value:0},{value:1},{value:2},{value:3},{value:4},{value:5},{value:6},{value:7},{value:8},{value:9}]},
			    						{name:'十位',nums:[{value:0},{value:1},{value:2},{value:3},{value:4},{value:5},{value:6},{value:7},{value:8},{value:9}]},
			    						{name:'个位',nums:[{value:0},{value:1},{value:2},{value:3},{value:4},{value:5},{value:6},{value:7},{value:8},{value:9}]}
		    						]
			    				},
			    				{name:'直选单式',code:'3DS',tip:'手动输入号码，输入 1 个三位数号码组成一注。',spec:'luru',
			    				regx:/^\d(,\d){2}(;\d(,\d){2})*$/,base:2000},
			    				{
			    					name:'直选和值',code:'3DT',tip:'从前三位和值 0-27 中任意选择 1 个或以上号码。',
			    					spec:'number',
			    					widget:false,
			    					regx:/^\d+(;\d+)*$/,base:2000,
			    					seperator:';',
			    					rows:[
			    						{name:'和值',code:'3##DT',nums:[{value:0},{value:1},{value:2},{value:3},{value:4},{value:5},{value:6},{value:7},{value:8},{value:9},
			    						{value:10},{value:11},{value:12},{value:13},{value:14},{value:15},{value:16},{value:17},{value:18},{value:19},
			    						{value:20},{value:21},{value:22},{value:23},{value:24},{value:25},{value:26},{value:27}]}
		    						]
			    				}
		    				]		
			    		},
			    		{
			    			name:'组选',
			    			methods:[
			    				{
			    					name:'组三',code:'3G3',tip:'从 0-9 中任意选择 2 个或以上号码。',
			    					spec:'number',
			    					widget:true,
			    					regx:/^\d(,\d)+$/,base:666.6667,
			    					rows:[
			    						{name:'',least:2,nums:[{value:0},{value:1},{value:2},{value:3},{value:4},{value:5},{value:6},{value:7},{value:8},{value:9}]}
		    						]
			    				},
			    				{
			    					name:'组六',code:'3G6',tip:'从 0-9 中选择 3 个或以上号码。',
			    					spec:'number',
			    					widget:true,
			    					regx:/^\d(,\d){2,}$/,base:329.8611,
			    					rows:[
			    						{name:'',least:3,nums:[{value:0},{value:1},{value:2},{value:3},{value:4},{value:5},{value:6},{value:7},{value:8},{value:9}]}
		    						]
			    				},
			    				{
			    					name:'混合组选',code:'3GM',tip:'手动输入号码，输入 1 个三位数号码组成一注。',
		    						mixedPrize:true,
		    						prizes:[
		    							{name:'组三',base:666.6667},
		    							{name:'组六',base:329.8611}
		    						],spec:'luru',
		    						regx:/^\d(,\d){2,}(;\d(,\d){2,})*$/,base:666.6667}
			    				]
			    			}
			    	]
		    	},
		    	{
		    		name:'二星',
		    		label:'erxing',
			    	methodgroups:[
			    		{
			    			name:'直选',
			    			methods:[
			    				{
			    					name:'前二直选复式',code:'2*DC',tip:' 分别从万位、千位中各选至少 1 个号码。',
			    					spec:'number',
			    					widget:true,
			    					regx:/^\d(,\d)*(;\d(,\d)*)+$/,base:200,
			    					rows:[
			    						{name:'万位',nums:[{value:0},{value:1},{value:2},{value:3},{value:4},{value:5},{value:6},{value:7},{value:8},{value:9}]},
			    						{name:'千位',nums:[{value:0},{value:1},{value:2},{value:3},{value:4},{value:5},{value:6},{value:7},{value:8},{value:9}]}
		    						]
		    					},
			    				{
			    					name:'前二直选单式',code:'2*DS',tip:' 手动输入号码，输入 1 个二位数号码组成一注。',spec:'luru',
			    					regx:/^\d,\d(;\d,\d)*$/,base:200
			    				},
			    				{
			    					name:'前二直选和值',code:'2*DT',tip:' 从前二位和值 0-18 中任意选择 1 个或以上号码。',
			    					spec:'number',
			    					widget:false,
			    					regx:/^\d+(;\d+)*$/,base:200,
			    					seperator:';',
			    					rows:[
			    						{name:'和值',nums:[{value:0},{value:1},{value:2},{value:3},{value:4},{value:5},{value:6},{value:7},{value:8},{value:9},
			    						{value:10},{value:11},{value:12},{value:13},{value:14},{value:15},{value:16},{value:17},{value:18}]}
		    						]
			    				},
			    				{
									name:'前二组选复式',code:'2*GC',tip:' 从 0-9 中任意选择 2 个或以上号码。',
			    					spec:'number',
			    					widget:true,
			    					regx:/^\d(,\d)+$/,base:100,
			    					rows:[
			    						{name:'组选',least:2,nums:[{value:0},{value:1},{value:2},{value:3},{value:4},{value:5},{value:6},{value:7},{value:8},{value:9}]}
		    						]
		    					},
			    				{name:'前二组选单式',code:'2*GS',tip:' 手动输入号码，输入 1 个二位数号码组成一注。',spec:'luru',
			    				regx:/^\d,\d(;\d,\d)*$/,base:100},
			    				{
			    					name:'前二组选和值',code:'2*GT',tip:' 从前二位和值 1-17 中任意选择 1 个或以上号码。',
			    					spec:'number',
			    					widget:false,
			    					regx:/^\d+(;\d+)*$/,base:100,
			    					seperator:';',
			    					rows:[
			    						{name:'和值',nums:[{value:1},{value:2},{value:3},{value:4},{value:5},{value:6},{value:7},{value:8},{value:9},
			    						{value:10},{value:11},{value:12},{value:13},{value:14},{value:15},{value:16},{value:17}]}
		    						]
			    				}
			    			]
			    		},
			    		{
			    			name:'组选',
			    			methods:[
			    			{
			    					name:'后二直选复式',code:'*2DC',tip:' 分别从十位、个位中各选至少 1 个号码。',
			    					spec:'number',
			    					widget:true,
			    					regx:/^\d(,\d)*(;\d(,\d)*)+$/,base:200,
			    					rows:[
			    						{name:'十位',nums:[{value:0},{value:1},{value:2},{value:3},{value:4},{value:5},{value:6},{value:7},{value:8},{value:9}]},
			    						{name:'个位',nums:[{value:0},{value:1},{value:2},{value:3},{value:4},{value:5},{value:6},{value:7},{value:8},{value:9}]}
		    						]
		    					},
			    				{name:'后二直选单式',code:'*2DS',tip:' 手动输入号码，输入 1 个二位数号码组成一注。',spec:'luru',
			    					regx:/^\d,\d(;\d,\d)*$/,base:200},
			    				{
			    					name:'后二直选和值',code:'*2DT',tip:' 从后二位和值 0-18 中任意选择 1 个或以上号码。',
			    					spec:'number',
			    					widget:false,
			    					regx:/^\d+(;\d+)*$/,base:200,
			    					seperator:';',
			    					rows:[
			    						{name:'和值',nums:[{value:0},{value:1},{value:2},{value:3},{value:4},{value:5},{value:6},{value:7},{value:8},{value:9},
			    						{value:10},{value:11},{value:12},{value:13},{value:14},{value:15},{value:16},{value:17},{value:18}]}
		    						]
			    				},
									{
			    					name:'后二组选复式',code:'*2GC',tip:' 从 0-9 中任意选择 2 个或以上号码。',
			    					spec:'number',
			    					widget:true,
			    					regx:/^\d(,\d)+$/,base:100,
			    					rows:[
			    						{name:'组选',least:2,nums:[{value:0},{value:1},{value:2},{value:3},{value:4},{value:5},{value:6},{value:7},{value:8},{value:9}]}
		    						]
		    					},
			    				{name:'后二组选单式',code:'*2GS',tip:' 手动输入号码，输入 1 个二位数号码组成一注。',spec:'luru',
			    				regx:/^\d,\d(;\d,\d)*$/,base:100},
			    				{
			    					name:'后二组选和值',code:'*2GT',tip:' 从前二位和值 1-17 中任意选择 1 个或以上号码。',
			    					spec:'number',
			    					widget:false,
			    					regx:/^\d+(;\d+)*$/,base:100,
			    					seperator:';',
			    					rows:[
			    						{name:'和值',nums:[{value:1},{value:2},{value:3},{value:4},{value:5},{value:6},{value:7},{value:8},{value:9},
			    						{value:10},{value:11},{value:12},{value:13},{value:14},{value:15},{value:16},{value:17}]}
		    						]
			    				}
		    				]
			    		}
			    	]
		    	},
		    	{
		    		name:'定位胆',
		    		label:'dingwei',
			    	methodgroups:[
			    		{
			    			name:'',
			    			methods:[
			    				{
			    					name:'定位胆',code:'1DP',tip:' 从百位、十位、个位任意1个位置或多个位置上选择1个号码。',
			    					spec:'number',
			    					widget:true,
			    					regx:/^(.{0}|(\d(,\d)*))(;(.{0}|(\d(,\d)*))){2}$/,base:20,
			    					rows:[
			    						{name:'百位',nums:[{value:0},{value:1},{value:2},{value:3},{value:4},{value:5},{value:6},{value:7},{value:8},{value:9}]},
			    						{name:'十位',nums:[{value:0},{value:1},{value:2},{value:3},{value:4},{value:5},{value:6},{value:7},{value:8},{value:9}]},
			    						{name:'个位',nums:[{value:0},{value:1},{value:2},{value:3},{value:4},{value:5},{value:6},{value:7},{value:8},{value:9}]}
		    						]
		    					}
			    			]
			    		}
			    	]
		    	},
		    	{
		    		name:'不定位',
			    	methodgroups:[
			    		{
			    			name:'三星不定位',
			    			methods:[
			    				{name:'一码不定位',code:'3F1',tip:' 从0-9中选择1个号码，每注由1个号码组成。',
			    					spec:'number',
			    					widget:true,
			    					regx:/^\d(;\d)*$/,base:7.0956,
			    					seperator:';',
			    					rows:[
			    						{name:'号码',nums:[{value:0},{value:1},{value:2},{value:3},{value:4},{value:5},{value:6},{value:7},{value:8},{value:9}]}
		    						]
		    					},
			    				{name:'二码不定位',code:'3F2',tip:'从0-9中选择2个号码，每注由2个号码组成。',
			    					spec:'number',
			    					widget:true,
			    					regx:/^\d(,\d)+$/,base:36.5355,
			    					rows:[
			    						{name:'号码',least:2,nums:[{value:0},{value:1},{value:2},{value:3},{value:4},{value:5},{value:6},{value:7},{value:8},{value:9}]}
		    						]
		    					}
			    			]
			    		}
			    	]
		    	},
		    	{
		    		name:'大小单双',
			    	methodgroups:[
			    		{
			    			name:'二星',
			    			methods:[
			    				{name:'后二大小单双',code:'*2S',tip:' 对十位、个位的“大（56789）小（01234）、单（13579）双（02468）”形态进行购买。',
			    					spec:'daxiaodanshuang',
			    					regx:/^\d(,\d)*;\d(,\d)*$/,base:8.0000,
			    					rows:[
			    						{name:'十位',nums:[{value:'大',trueValue:0},{value:'小',trueValue:1},{value:'单',trueValue:2},{value:'双',trueValue:3}]},
			    						{name:'个位',nums:[{value:'大',trueValue:0},{value:'小',trueValue:1},{value:'单',trueValue:2},{value:'双',trueValue:3}]}
		    						]
		    					},
			    				{name:'前二大小单双',code:'2*S',tip:' 对百位、十位的“大（56789）小（01234）、单（13579）双（02468）”形态进行购买。',
			    					spec:'daxiaodanshuang',
			    					regx:/^\d(,\d)*;\d(,\d)*$/,base:8.0000,
			    					rows:[
			    						{name:'百位',nums:[{value:'大',trueValue:0},{value:'小',trueValue:1},{value:'单',trueValue:2},{value:'双',trueValue:3}]},
			    						{name:'十位',nums:[{value:'大',trueValue:0},{value:'小',trueValue:1},{value:'单',trueValue:2},{value:'双',trueValue:3}]}
		    						]
		    					},
			    				{name:'三码大小单双',code:'3S',tip:' 从百位、十位、个位中的“大、小、单、双”中至少各选一个组成一注。。',
			    					spec:'daxiaodanshuang',
			    					regx:/^\d(,\d)*;\d(,\d)*;\d(,\d)*$/,base:16.0000,
			    					rows:[
			    						{name:'百位',nums:[{value:'大',trueValue:0},{value:'小',trueValue:1},{value:'单',trueValue:2},{value:'双',trueValue:3}]},
			    						{name:'十位',nums:[{value:'大',trueValue:0},{value:'小',trueValue:1},{value:'单',trueValue:2},{value:'双',trueValue:3}]},
			    						{name:'个位',nums:[{value:'大',trueValue:0},{value:'小',trueValue:1},{value:'单',trueValue:2},{value:'双',trueValue:3}]}
		    						]
		    					}
			    			]
			    		}
			    		
			    	]
		    	}
	    	]
	    };
lottos.fc3d =fc3d,lottos.fc3d.adjustTime=540;
var pl3 =angular.copy(window.fc3d);
 pl3.name='体彩排三',pl3.label='PL3',pl3.adjustTime=19*60;
lottos.pl3 = pl3;
var pl3 =angular.copy(fc3d);
 pl3.name='极速3D',pl3.label='FFC3D5',pl3.isOur=true;
lottos.ffc3d5 = pl3;
var pl3 =angular.copy(fc3d);
 pl3.name='激情排三',pl3.label='FFCPL35',pl3.isOur=true;
lottos.ffcpl35 = pl3;